# fri:day code challenge

An address provider returns addresses only with concatenated street names and numbers. Our own system on the other hand has separate fields for street name and street number.

Input: string of address
Output: string of street and string of street-number

Clone it and either load into your favorite IDE or use maven directly.

# Command Line
You can run the application in your machine by running the jar file: addressparser-0.1.0-SNAPSHOT.jar
To do this, open three CMD windows (Windows) or three Terminal windows (MacOS, Linux) and arrange so you can view them conveniently.

 1. In each window, change to the directory where you placed the source directory
 2. In the first window, build the application using `mvn clean package`, this will generate a jar file into a target folder.
 3. If you dont want to build and compile the code, just copy the jar file to some /your-machine/path/ 
 4. In the same window run: `java -jar target/addressparser-0.1.0-SNAPSHOT.jar
 5. Then you will have to enter some address direction to see how the application works.
 
# Behaviour
The application can parse different kind of addresses, I categorized this types into: simple, complex and bonus types.
For the Bonus one, I've defined prefefined formats for the following countries: US, France and Argentina.

##Simple 
1. input: "Winterallee 3" ->  output: {"Winterallee", "3"}
2. input: "Blaufeldweg 123B" -> output: {"Blaufeldweg", "123B"}

##Complex
1. input: "Am Bächle 23" -> output: {"Am Bächle", "23"}
2. input: "Auf der Vogelwiese 23 b" -> output: {"Auf der Vogelwiese", "23 b"}

##Bonus
1. input: "4, rue de la revolution" -> output: {"rue de la revolution", "4"}
2. input: "200 Broadway Av" -> output: {"Broadway Av", "200"}
3. input: "Calle Aduana, 29" -> output: {"Calle Aduana", "29"}
4. input: "Calle 39 No 1540" -> output: {"Calle 39", "No 1540"}

##Errors
In case that you entered a non valid address format, the application will show the message: "Address is invalid!"

#Improvement and comments
1. No logging used
2. Could be think in a way to configure a list of countries and its addresses formats, then handle this with a kind of global configuration file.
3. I've implemented The strategy pattern to give a different parse treatment to each type of address. 
