package com.addressparser.app;

import java.util.regex.Pattern;

public abstract class AddressUtil {

	public static Pattern SIMPLE_ADDRESS_REGEX = Pattern.compile("^([\\w*\\s,.-]+?).([1-9]?[0-9][\\w*]{0,4})*$");
	public static Pattern COMPLEX_ADDRESS_REGEX = Pattern.compile("^([\\w*äöüß\\s,.-]+?).([1-9][0-9\\s\\w*,.-]{0,9})*$");
	public static Pattern FRANCE_BONUS_ADDRESS_REGEX = Pattern.compile("^([1-9][0-9\\s]{0,5}).([\\w*àâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇ\\s]+)*$");
	public static Pattern US_BONUS_ADDRESS_REGEX = Pattern.compile("^([1-9][0-9]{0,4}).([\\s\\w*\\s]+)*$");
	public static Pattern ARG_BONUS_ADDRESS_REGEX_A = Pattern.compile("^([\\w*\\s.-]+?),.([\\w*\\s]+?[1-9]?[0-9\\s,.-]{0,4}[\\w*\\s])*$");
	public static Pattern ARG_BONUS_ADDRESS_REGEX_B = Pattern.compile("^([\\w*\\s,.-]*)(No).([\\s\\w*\\s]+?[1-9][0-9\\s\\w*,.-]{0,4})*$");

	public static boolean isSimpleAddress(String address) {
		return SIMPLE_ADDRESS_REGEX.matcher(address).matches();
	}

	public static boolean isComplexAddress(String address) {
		return COMPLEX_ADDRESS_REGEX.matcher(address).matches();
	}

	public static boolean isBonusAddress(String address) {
		return (ARG_BONUS_ADDRESS_REGEX_A.matcher(address).matches()
				|| ARG_BONUS_ADDRESS_REGEX_B.matcher(address).matches()
				|| FRANCE_BONUS_ADDRESS_REGEX.matcher(address).matches()
				|| US_BONUS_ADDRESS_REGEX.matcher(address).matches());
	}

	public static Pattern getValidPatternByAddress(String address) {
		if (ARG_BONUS_ADDRESS_REGEX_B.matcher(address).matches()) {
			return ARG_BONUS_ADDRESS_REGEX_B;
		} else if (US_BONUS_ADDRESS_REGEX.matcher(address).matches()) {
			return US_BONUS_ADDRESS_REGEX;
		} else if (FRANCE_BONUS_ADDRESS_REGEX.matcher(address).matches()) {
			return FRANCE_BONUS_ADDRESS_REGEX;
		} else if (ARG_BONUS_ADDRESS_REGEX_A.matcher(address).matches()) {
			return ARG_BONUS_ADDRESS_REGEX_A;
		} else {
			return null;
		}

	}

}
