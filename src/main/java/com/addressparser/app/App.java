package com.addressparser.app;

import java.io.InputStream;
import java.util.Scanner;

import com.addressparser.exception.InvalidAddressException;
import com.addressparser.strategy.AddressParserStrategy;
import com.addressparser.strategy.BonusParserStrategy;
import com.addressparser.strategy.ComplexParserStrategy;
import com.addressparser.strategy.SimpleParserStrategy;

/**
 * Address parser that return string of street and string of street-number from
 * an entered string address
 */
public class App {

	private static AddressParserStrategy strategy;

	public static void main(String[] address) {
		System.out.println("Address parser split an entered string address into String of street name and string of street number");
		InputStream stream = System.in;
		System.out.println("Please enter an address to parse or exit to finish the application.");
		Scanner scanner = new Scanner(stream);
		while (scanner.hasNextLine()) {
			try {
				String input = scanner.nextLine();
				if (input.equals("exit")) {
					scanner.close();
					break;
				}
				setStrategyByAddress(input);
				System.out.println("output:" + strategy.parseAddress(input));
			} catch (InvalidAddressException e) {
				System.out.println("Invalid address, please try again or press exit to quit.");
			}
		}
	}

	private static void setStrategyByAddress(String input) throws InvalidAddressException {
		if (AddressUtil.isBonusAddress(input)) {
			strategy = new BonusParserStrategy();
		}  else if (AddressUtil.isComplexAddress(input)) {
			strategy = new ComplexParserStrategy();
		} else if (AddressUtil.isSimpleAddress(input)) {
			strategy = new SimpleParserStrategy();
		} else {
			throw new InvalidAddressException("Address is not valid!");
		}
	}

}
