package com.addressparser.app.model;

public class Address {

	private String streeetName;
	private String streetNumber;

	public Address(final String streetName, final String streetNumber) {
		this.streeetName = streetName.trim();
		this.streetNumber = streetNumber.trim();
	}

	public String getStreeetName() {
		return streeetName;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public String toString() {
		return "{'" + this.streeetName + "','" + this.streetNumber + "'}";
	}

}
