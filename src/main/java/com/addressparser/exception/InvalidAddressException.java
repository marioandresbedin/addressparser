package com.addressparser.exception;

public class InvalidAddressException extends Exception {

	private static final long serialVersionUID = 5355077057234408136L;
	public static String INVALID_ADDRESS_ERROR = "Address is invalid!";


	public InvalidAddressException(String message) {
		super(message);
	}

}
