package com.addressparser.strategy;

import com.addressparser.exception.InvalidAddressException;

public interface AddressParserStrategy {
	
	String parseAddress(String address) throws InvalidAddressException;

}
