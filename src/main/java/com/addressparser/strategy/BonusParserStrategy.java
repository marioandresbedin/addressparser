package com.addressparser.strategy;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.addressparser.app.AddressUtil;
import com.addressparser.app.model.Address;
import com.addressparser.exception.InvalidAddressException;

public class BonusParserStrategy implements AddressParserStrategy {

	public String parseAddress(String addressStr) throws InvalidAddressException {
		Pattern bonusParser = AddressUtil.getValidPatternByAddress(addressStr);
		if (bonusParser == null) throw new InvalidAddressException(InvalidAddressException.INVALID_ADDRESS_ERROR);
		
		Matcher addressNameMatcher = bonusParser.matcher(addressStr);
		if (!addressNameMatcher.matches()) throw new InvalidAddressException(InvalidAddressException.INVALID_ADDRESS_ERROR);
		
		Address address = getAddress(addressNameMatcher);
		if (address == null) throw new InvalidAddressException(InvalidAddressException.INVALID_ADDRESS_ERROR);
		
		return address.toString();
	}
	
	private static Address getAddress(Matcher addressNameMatcher) throws InvalidAddressException {
		String addressName = null;
		String addressNumber = null;
		if (addressNameMatcher.pattern().equals(AddressUtil.US_BONUS_ADDRESS_REGEX)){
			addressName = addressNameMatcher.group(2);
			addressNumber = addressNameMatcher.group(1);
		} else if (addressNameMatcher.pattern().equals(AddressUtil.ARG_BONUS_ADDRESS_REGEX_B)) {
			addressName = addressNameMatcher.group(1);
			addressNumber = addressNameMatcher.group(2).concat(" ").concat(addressNameMatcher.group(3));
		} else {
			addressName = addressNameMatcher.group(1);
			addressNumber = addressNameMatcher.group(2);
		}
		if (addressName == null || addressName.isEmpty() || addressNumber == null || addressNumber.isEmpty()) {
			throw new InvalidAddressException(InvalidAddressException.INVALID_ADDRESS_ERROR);
		}
		return new Address(addressName, addressNumber);

	}

}
