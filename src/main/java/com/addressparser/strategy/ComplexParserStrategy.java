package com.addressparser.strategy;

import java.util.regex.Matcher;

import com.addressparser.app.AddressUtil;
import com.addressparser.app.model.Address;
import com.addressparser.exception.InvalidAddressException;

public class ComplexParserStrategy implements AddressParserStrategy {

	public String parseAddress(String addressStr) throws InvalidAddressException {
		Matcher addressNameMatcher = AddressUtil.COMPLEX_ADDRESS_REGEX.matcher(addressStr);
		if (!addressNameMatcher.matches()) throw new InvalidAddressException(InvalidAddressException.INVALID_ADDRESS_ERROR);
		
		String addressName = addressNameMatcher.group(1);
		String addressNumber = addressNameMatcher.group(2);
		
		if (addressName == null || addressName.isEmpty() || addressNumber == null || addressNumber.isEmpty()) {
			throw new InvalidAddressException(InvalidAddressException.INVALID_ADDRESS_ERROR);
		}
		
		Address address = new Address(addressName, addressNumber);
		return address.toString();
	}

}
