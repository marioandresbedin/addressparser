package com.addressparser.strategy;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.mockito.Mockito;

import com.addressparser.exception.InvalidAddressException;

public class AddressParserStrategyTest {

	@Test
	public void testSimpleParseAddress() throws InvalidAddressException {
		AddressParserStrategy apStrategy = Mockito.mock(SimpleParserStrategy.class);

		Mockito.when(apStrategy.parseAddress("Winterallee 3")).thenCallRealMethod();
		assertThat(apStrategy.parseAddress("Winterallee 3"), is("{'Winterallee','3'}"));

		Mockito.when(apStrategy.parseAddress("Musterstrasse 45")).thenCallRealMethod();
		assertThat(apStrategy.parseAddress("Musterstrasse 45"), is("{'Musterstrasse','45'}"));

		Mockito.when(apStrategy.parseAddress("Blaufeldweg 123B")).thenCallRealMethod();
		assertThat(apStrategy.parseAddress("Blaufeldweg 123B"), is("{'Blaufeldweg','123B'}"));
	}

	@Test(expected = InvalidAddressException.class)
	public void testSimpleParseAddressError() throws InvalidAddressException {
		AddressParserStrategy apStrategy = Mockito.mock(SimpleParserStrategy.class);
		Mockito.when(apStrategy.parseAddress("Musterstrasse")).thenCallRealMethod();
		apStrategy.parseAddress("Musterstrasse");
	}

	@Test
	public void testComplexParseAddress() throws InvalidAddressException {
		AddressParserStrategy apStrategy = Mockito.mock(ComplexParserStrategy.class);

		Mockito.when(apStrategy.parseAddress("Am Bächle 23")).thenCallRealMethod();
		assertThat(apStrategy.parseAddress("Am Bächle 23"), is("{'Am Bächle','23'}"));

		Mockito.when(apStrategy.parseAddress("Auf der Vogelwiese 23 b")).thenCallRealMethod();
		assertThat(apStrategy.parseAddress("Auf der Vogelwiese 23 b"), is("{'Auf der Vogelwiese','23 b'}"));

	}

	@Test(expected = InvalidAddressException.class)
	public void testComplexParseAddressError() throws InvalidAddressException {
		AddressParserStrategy apStrategy = Mockito.mock(SimpleParserStrategy.class);
		Mockito.when(apStrategy.parseAddress("Am Bächle")).thenCallRealMethod();
		apStrategy.parseAddress("Am Bächle");
	}

	@Test
	public void testBonusParseAddress() throws InvalidAddressException {
		AddressParserStrategy apStrategy = Mockito.mock(BonusParserStrategy.class);
		// France format
		Mockito.when(apStrategy.parseAddress("4, rue de la revolution")).thenCallRealMethod();
		assertThat(apStrategy.parseAddress("4, rue de la revolution"), is("{'rue de la revolution','4'}"));

		// Us format
		Mockito.when(apStrategy.parseAddress("200 Broadway Av")).thenCallRealMethod();
		assertThat(apStrategy.parseAddress("200 Broadway Av"), is("{'Broadway Av','200'}"));

		// Argentina format
		Mockito.when(apStrategy.parseAddress("Calle Aduana, 29")).thenCallRealMethod();
		assertThat(apStrategy.parseAddress("Calle Aduana, 29"), is("{'Calle Aduana','29'}"));

		Mockito.when(apStrategy.parseAddress("Calle 123 No 1540")).thenCallRealMethod();
		assertThat(apStrategy.parseAddress("Calle 123 No 1540"), is("{'Calle 123','No 1540'}"));

	}

}
